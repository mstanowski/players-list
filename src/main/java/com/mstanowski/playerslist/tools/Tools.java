package com.mstanowski.playerslist.tools;

public class Tools {

	public static int checkGoals(Integer goals) {
		int lv_goals;

		if (goals == null) {
			lv_goals = 1;
		} else {
			lv_goals = goals;
		}

		return lv_goals;
	}

}
