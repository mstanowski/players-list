package com.mstanowski.playerslist.domain;

public class PlayerId {
	private String playerId;
	
	public PlayerId() {};
	public PlayerId(String playerId) {
		this.playerId = playerId;
	};

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	
	

}
