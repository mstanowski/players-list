package com.mstanowski.playerslist.domain;

public class PlayerStatistics {
	private int goals;
	private String firstName;
	private String lastName;
	private String playerId;
	
	public PlayerStatistics(int goals, String firstName, String lastName,String playerId) {
		this.goals = goals;
		this.firstName = firstName;
		this.lastName = lastName;
		this.playerId = playerId;
	}
	
	public PlayerStatistics() {};
	
	public int getGoals() {
		return goals;
	}
	public void setGoals(int goals) {
		this.goals = goals;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSecondName() {
		return lastName;
	}
	public void setSecondName(String secondName) {
		this.lastName = secondName;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	
}
