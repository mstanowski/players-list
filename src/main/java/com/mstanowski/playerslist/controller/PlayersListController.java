package com.mstanowski.playerslist.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mstanowski.playerslist.domain.PlayerId;
import com.mstanowski.playerslist.domain.PlayerStatistics;
import com.mstanowski.playerslist.entity.Player;
import com.mstanowski.playerslist.entity.PlayerDAO;
import com.mstanowski.playerslist.entity.Season;
import com.mstanowski.playerslist.entity.SeasonDAO;
import com.mstanowski.playerslist.entity.Statistics;
import com.mstanowski.playerslist.services.PlayersListService;
import com.mstanowski.playerslist.services.PlayersListServiceImpl;
import com.mstanowski.playerslist.tools.Tools;

@RestController
public class PlayersListController {

	private final PlayersListService playersListService;

	@Autowired
	public PlayersListController(PlayersListService playerService) {
		this.playersListService = playerService;
	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@RequestMapping(value = "/addingNewPlayer", method = { RequestMethod.POST })
	public PlayerId addPlayer(@RequestParam String firstName, @RequestParam String lastName,
			@RequestParam(name = "goals", required = false) Integer goals) {
		Statistics statistics = new Statistics();
		Player player = new Player();
		PlayerId playerId = new PlayerId();

		statistics.setGoals(Tools.checkGoals(goals));
		player.setFirstName(firstName);
		player.setLastName(lastName);

		playerId.setPlayerId(playersListService.addPlayer(player, statistics).getId());

		return playerId;
	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@RequestMapping(value = "/updatePlayer", method = { RequestMethod.POST })
	public PlayerStatistics updatePlayer(@RequestParam String playerId,
			@RequestParam(name = "goals", required = false) Integer goals) {
		Player player = playersListService.updatePlayer(playerId, Tools.checkGoals(goals));
		if (player != null) {
			PlayerStatistics ps = new PlayerStatistics(player.getStatistics().getGoals(), player.getFirstName(),
					player.getLastName(), player.getId());

			return ps;
		}
		
		return null;

	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@RequestMapping(value = "/removePlayer", method = { RequestMethod.POST })
	public boolean removePlayer(@RequestParam String playerId) {
		return playersListService.removePlayer(playerId);
	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@RequestMapping(value = "/playersList", method = { RequestMethod.GET })
	public List<PlayerStatistics> playersList(@RequestParam int year) {

		List<PlayerStatistics> playersStaistics = new ArrayList<PlayerStatistics>();
		playersListService.getStatisticsForSeason(year).forEach(Statistics -> {
			PlayerStatistics ps = new PlayerStatistics(Statistics.getGoals(), Statistics.getPlayer().getFirstName(),
					Statistics.getPlayer().getLastName(), Statistics.getPlayer().getId());
			playersStaistics.add(ps);
		});

		return playersStaistics;
	}

}