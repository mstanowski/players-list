package com.mstanowski.playerslist.entity;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SeasonDAO extends CrudRepository<Season,Long >{

	@Transactional
	Season save(Season season);
	Season findByYear(int year);
	
}
