package com.mstanowski.playerslist.entity;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.domain.Sort;

public interface StatisticsDAO extends CrudRepository<Statistics,Long >{
	
	Statistics save(Statistics statistics);
	Statistics findBySeasonAndPlayer(Season season, Player player);
	List<Statistics> findBySeason(Season season, Sort sort);

}
