package com.mstanowski.playerslist.entity;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerDAO extends CrudRepository<Player,Long >{

	Player save(Player player);
	Optional<Player> findById(String id);
	void delete(Player player);
}
