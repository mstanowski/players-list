package com.mstanowski.playerslist.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "season")
@SequenceGenerator(name="seq", initialValue=1, allocationSize=100)
public class Season {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
	private long season_id;
	@Column(name = "year")
	private int year;
	@OneToMany(
			mappedBy = "season",
	        cascade = CascadeType.ALL,
	        orphanRemoval = true
	    )
	private List<Statistics> statistics = new ArrayList<>();
	
	public long getId() {
		return season_id;
	}
	public void setId(long id) {
		this.season_id = id;
	}
	public int getSeason() {
		return year;
	}
	public void setSeason(int season) {
		this.year = season;
	}
	public List<Statistics> getStatistics() {
		return statistics;
	}
	public void setStatistics(List<Statistics> statistics) {
		this.statistics = statistics;
	}

}