package com.mstanowski.playerslist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlayersList2Application {

	public static void main(String[] args) {
		SpringApplication.run(PlayersList2Application.class, args);
	}

}
