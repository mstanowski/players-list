package com.mstanowski.playerslist.services;

import java.util.List;

import com.mstanowski.playerslist.entity.Player;
import com.mstanowski.playerslist.entity.Season;
import com.mstanowski.playerslist.entity.Statistics;

public interface PlayersListService {

	Player addPlayer(Player player, Statistics statistics);
	Player getPlayer(String playerId);
	Player updatePlayer(String playerId,int goals);
	boolean removePlayer(String playerId);
	List<Statistics> getStatisticsForSeason(int year);
}
