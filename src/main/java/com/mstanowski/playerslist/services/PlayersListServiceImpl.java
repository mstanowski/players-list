package com.mstanowski.playerslist.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.mstanowski.playerslist.entity.Player;
import com.mstanowski.playerslist.entity.PlayerDAO;
import com.mstanowski.playerslist.entity.SeasonDAO;
import com.mstanowski.playerslist.entity.Statistics;
import com.mstanowski.playerslist.entity.StatisticsDAO;
import com.mstanowski.playerslist.entity.Season;


@Service
public class PlayersListServiceImpl implements PlayersListService{
	
	private final SeasonDAO seasonDao;
	private final PlayerDAO playerDao;
	private final StatisticsDAO statisticsDao;
	
	@Autowired
	public PlayersListServiceImpl(SeasonDAO seasonDao, PlayerDAO playerDao, StatisticsDAO statisticsDao) {
		this.seasonDao = seasonDao;
		this.playerDao = playerDao;
		this.statisticsDao = statisticsDao;
	}
	
	@Override
	public Player addPlayer(Player player, Statistics statistics) {
		
		Season season = seasonDao.findByYear(Calendar.getInstance().get(Calendar.YEAR));
		
		if (season == null) {
			season = new Season();
			season.setSeason(Calendar.getInstance().get(Calendar.YEAR));
		}
		statistics.setPlayer(player);
		statistics.setSeason(season);
		season.getStatistics().add(statistics);
		
		playerDao.save(player);
		seasonDao.save(season);
		
		return player;
		
	}

	@Override
	public Player getPlayer(String playerId) {
		Player player = playerDao.findById(playerId).orElse(null);;
		return player;
	}

	@Override
	public Player updatePlayer(String playerId, int goals) {
		Player player = playerDao.findById(playerId).orElse(null);
		
		if (player != null) {
			Season season = seasonDao.findByYear(Calendar.getInstance().get(Calendar.YEAR));
			Statistics statistics = statisticsDao.findBySeasonAndPlayer(season, player);			
			statistics.setGoals(statistics.getGoals() + goals);
			statisticsDao.save(statistics);
		}

		return player;
	}

	@Override
	public boolean removePlayer(String playerId) {
		Player player = playerDao.findById(playerId).orElse(null);
		
		if (player != null) {
			playerDao.delete(player);
			return true;
		}
		
		return false;
	}

	@Override
	public List<Statistics> getStatisticsForSeason(int year) {
		Season season = seasonDao.findByYear(year);
		return statisticsDao.findBySeason(season, Sort.by("goals").descending());
	}

}
