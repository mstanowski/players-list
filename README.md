customer specification:
we would like you to write a REST application supporting our local football league, it will be quite simple in fact, consisting of following end points:
- list of all players - each entry should consist of player’s name, ID (please, suggest the best type to use), 
and number of goals in current season; the list should be sorted by number of goals, descendant (it will be top shooter list)
- adding new player to the list, it will accept player’s name, and initial number of goals (optional, 1 by default), returning player ID
- updating player - it will accept player ID, and number of goals to be added to player’s score (optional, 1 by default)
- removing the player from the list - it will accept player ID as an input


Project name: Players List.


The application contains the following endpoints:

GET method:
* /playersList?year=\<season year>  - return list of players in JSON format. Each
player entity contains id by pattern: 
    player_<5 digits starting with 00001 and increasing by 1>

POST methods:    
* /addingNewPlayer?firstName=\<name>&lastName=\<lastName>&goals=\<number of goals> 
-return created player id in JSON format. Goals parameter are optional (1 by default).

* /updatePlayer?playerId=player_<5 digits>&goals=\<number of goals> - return 
player data with current statistics. Goals parameter are optional (1 by default).

* /removePlayer?playerId=player_<5 digits> - return true if removal operation 
finished with successful or false if not.



Extra endpoint for database preview:
* /console